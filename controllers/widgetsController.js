const teklifModel = require("../models/teklifModel");

exports.widget2 = async (req, res) => {
    return res.status(200).json({
        'title' : 'Overdue',
        'data'  : {
            'label': 'TASKS',
            'count': 4,
            'extra': {
                'label': 'Yesterday\'s overdue',
                'count': 2
            }
        },
        'detail': 'You can show some detailed information about this widget in here.'
    })
}

exports.widgetTeklifler = async (req, res) => {
    return res.status(200).json(
        {
            'title': 'Teklifler',
            'table': {
                'columns': [
                    {
                        'id'   : 'budget_type',
                        'title': 'Budget Type'
                    },
                    {
                        'id'   : 'total_budget',
                        'title': 'Total Budget'
                    },
                    {
                        'id'   : 'spent_usd',
                        'title': 'Spent ($)'
                    },
                    {
                        'id'   : 'spent_perc',
                        'title': 'Spent (%)'
                    },
                    {
                        'id'   : 'remaining_usd',
                        'title': 'Remaining ($)'
                    },
                    {
                        'id'   : 'remaining_perc',
                        'title': 'Remaining (%)'
                    }
                ],
                'rows'   : [
                    {
                        'id'   : 1,
                        'cells': [
                            {
                                'id'     : 'budget_type',
                                'value'  : 'Wireframing',
                                'classes': 'bg-blue text-white',
                                'icon'   : ''
                            },
                            {
                                'id'     : 'total_budget',
                                'value'  : '$14,880.00',
                                'classes': 'font-bold',
                                'icon'   : ''
                            },
                            {
                                'id'     : 'spent_usd',
                                'value'  : '$14,000.00',
                                'classes': '',
                                'icon'   : ''
                            },
                            {
                                'id'     : 'spent_perc',
                                'value'  : '%94.08',
                                'classes': 'text-green',
                                'icon'   : 'trending_up'
                            },
                            {
                                'id'     : 'remaining_usd',
                                'value'  : '$880.00',
                                'classes': '',
                                'icon'   : ''
                            },
                            {
                                'id'     : 'remaining_perc',
                                'value'  : '%5.92',
                                'classes': '',
                                'icon'   : ''
                            }
                        ]
                    },
                    {
                        'id'   : 2,
                        'cells': [
                            {
                                'id'     : 'budget_type',
                                'value'  : 'Design',
                                'classes': 'bg-green text-white',
                                'icon'   : ''
                            },
                            {
                                'id'     : 'total_budget',
                                'value'  : '$21,080.00',
                                'classes': 'font-bold',
                                'icon'   : ''
                            },
                            {
                                'id'     : 'spent_usd',
                                'value'  : '$17,240.34',
                                'classes': '',
                                'icon'   : ''
                            },
                            {
                                'id'     : 'spent_perc',
                                'value'  : '%81.78',
                                'classes': 'text-green',
                                'icon'   : 'trending_up'
                            },
                            {
                                'id'     : 'remaining_usd',
                                'value'  : '$3,839.66',
                                'classes': '',
                                'icon'   : ''
                            },
                            {
                                'id'     : 'remaining_perc',
                                'value'  : '%18.22',
                                'classes': '',
                                'icon'   : ''
                            }
                        ]
                    },
                    {
                        'id'   : 3,
                        'cells':
                            [
                                {
                                    'id'     : 'budget_type',
                                    'value'  : 'Coding',
                                    'classes': 'bg-red text-white',
                                    'icon'   : ''
                                },
                                {
                                    'id'     : 'total_budget',
                                    'value'  : '$34,720.00',
                                    'classes': 'font-bold',
                                    'icon'   : ''
                                },
                                {
                                    'id'     : 'spent_usd',
                                    'value'  : '$3,518.00',
                                    'classes': '',
                                    'icon'   : ''
                                },
                                {
                                    'id'     : 'spent_perc',
                                    'value'  : '%10.13',
                                    'classes': 'text-red',
                                    'icon'   : 'trending_down'
                                },
                                {
                                    'id'     : 'remaining_usd',
                                    'value'  : '$31,202.00',
                                    'classes': '',
                                    'icon'   : ''
                                },
                                {
                                    'id'     : 'remaining_perc',
                                    'value'  : '%89.87',
                                    'classes': '',
                                    'icon'   : ''
                                }
                            ]
                    },
                    {
                        'id'   : 4,
                        'cells':
                            [
                                {
                                    'id'     : 'budget_type',
                                    'value'  : 'Marketing',
                                    'classes': 'bg-pink text-white',
                                    'icon'   : ''
                                },
                                {
                                    'id'     : 'total_budget',
                                    'value'  : '$34,720.00',
                                    'classes': 'font-bold',
                                    'icon'   : ''
                                },
                                {
                                    'id'     : 'spent_usd',
                                    'value'  : '$0.00',
                                    'classes': '',
                                    'icon'   : ''
                                },
                                {
                                    'id'     : 'spent_perc',
                                    'value'  : '%0.00',
                                    'classes': 'text-blue',
                                    'icon'   : 'trending_flat'
                                },
                                {
                                    'id'     : 'remaining_usd',
                                    'value'  : '$34,720.00',
                                    'classes': '',
                                    'icon'   : ''
                                },
                                {
                                    'id'     : 'remaining_perc',
                                    'value'  : '%100.00',
                                    'classes': '',
                                    'icon'   : ''
                                }
                            ]
                    },
                    {
                        'id'   : 5,
                        'cells':
                            [
                                {
                                    'id'     : 'budget_type',
                                    'value'  : 'Extra',
                                    'classes': 'bg-orange text-white',
                                    'icon'   : ''
                                },
                                {
                                    'id'     : 'total_budget',
                                    'value'  : '$18,600.00',
                                    'classes': 'font-bold',
                                    'icon'   : ''
                                },
                                {
                                    'id'     : 'spent_usd',
                                    'value'  : '$0.00',
                                    'classes': '',
                                    'icon'   : ''
                                },
                                {
                                    'id'     : 'spent_perc',
                                    'value'  : '%0.00',
                                    'classes': 'text-blue',
                                    'icon'   : 'trending_flat'
                                },
                                {
                                    'id'     : 'remaining_usd',
                                    'value'  : '$34,720.00',
                                    'classes': '',
                                    'icon'   : ''
                                },
                                {
                                    'id'     : 'remaining_perc',
                                    'value'  : '%100.00',
                                    'classes': '',
                                    'icon'   : ''
                                }
                            ]
                    }
                ]
            }    
        }
    )
}

exports.widgetOnayBekleyenTeklifler = async (req, res) => {
    var onayBekleyenTeklifler = await teklifModel.find({TeklifDurumu: 0})
        .populate('Firma', 'FirmaMarkasi')
        .populate('Urun', 'Adi')
        .populate('MusteriTemsilcisi', 'Adi')
    console.log(onayBekleyenTeklifler)
    return req.returnTemplate(onayBekleyenTeklifler, "")
}
