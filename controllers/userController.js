var _ = require('lodash');
const userModel = require("../models/userModel");
const command = require("../command");
const jwt = require('jsonwebtoken');
const config = require('../config');

exports.auth = async (req, res, next) => {
    const data              = req.body.data;
    const {email, password} = data;

    const user = await userModel.findOne( {KullaniciAdi : email})
    let verified = command.VerifyHash(password, user.Password);

    const error = {
        email   : user ? null : 'Check your username/email',
        password: user && verified ? null : 'Check your password'
    };

    if ( !error.email && !error.password && !error.displayName )
    {
        var _user =         { // TODO
            uuid    : '',
            from    : '',
            password: "admin",
            role    : "admin",
            data    : {
                'displayName': '',
                'photoURL'   : 'assets/images/avatars/Abbott.jpg',
                'email'      : 'admin',
                settings     : {
                    layout          : {
                        style : 'layout1',
                        config: {
                            scroll : 'content',
                            navbar : {
                                display : true,
                                folded  : true,
                                position: 'left'
                            },
                            toolbar: {
                                display : true,
                                style   : 'fixed',
                                position: 'below'
                            },
                            footer : {
                                display : true,
                                style   : 'fixed',
                                position: 'below'
                            },
                            mode   : 'fullwidth'
                        }
                    },
                    customScrollbars: true,
                    theme           : {
                        main   : 'mainThemeLight',
                        navbar : 'mainThemeLight',
                        toolbar: 'mainThemeLight',
                        footer : 'mainThemeLight'
                    }
                },
                shortcuts    : [
                    'calendar',
                    'mail',
                    'contacts'
                ]
            }
        }

        delete _user['password'];

        const access_token = jwt.sign({id: _user.uuid}, config.secret, {expiresIn: config.expiresIn});

        const response = {
            "user"        : _user,
            "access_token": access_token
        };

        return res.status(200).json(response)
    }
    else
    {
        return res.status(200).json({error})
    }
}

exports.accessToken = async (req, res) => {
    const {access_token} = req.body.data
    try {
        const {id} = jwt.verify(access_token, config.secret)
        const user = _.cloneDeep(userModel.findOne(user => user.id === id))
        delete user['password']
        const updatedAccessToken = jwt.sign({id: user.id}, config.secret, {expiresIn: config.expiresIn})
        const response = {
            user: user,
            access_token: updatedAccessToken
        }
        res.status(200).json(response)
    }
    catch(e) {
        //console.log(e)
    }
}

exports.list = async (req, res) => {
    if (req.query.userId) {
        let row = await userModel.findOne({_id: req.query.userId})
        return req.returnTemplate(row, "", 200)
    }
    else {
        let row = await userModel.find()
        return req.returnTemplate(row, "", 200)
    }
};




exports.create = async (req, res) => {
    let payload = req.body;
    var new_userModel = new userModel(payload);
    let row = await new_userModel.save()
    //  new_userModel.save((err, row) => {
    //     if (err) {
    //       res.status(500).send(err);
    //     }
    //     res.status(201).json(row);
    //   });  
    return req.returnTemplate(row, "")
};

exports.getById = async (req, res) => {
    let row = await userModel.find({ _id: req.params.id });
    // userModel.find({ _id: req.params.id }, (err, row) => {
    //   if (err) {
    //     res.status(500).send(err);
    //   }
    //   res.status(200).json(row);
    // });
    return req.returnTemplate(row, "")
};

exports.update = async (req, res) => {
    userModel.findOneAndUpdate(
        { _id: req.params.id },
        req.body,
        { new: true },
        (err, row) => {
            if (err) {
                return req.returnTemplate([], err, 500);
            }
            return req.returnTemplate(row, "")
        }
    );
};

exports.delete = async (req, res) => {
    userModel.remove({ _id: req.params.id }, (err, Category) => {
        if (err) {
            return req.returnTemplate([], err, 404);
        }
        return req.returnTemplate([], "başarı ile silinmiştir")
    });
};


// async function getAltUserId(array, id) {
//     let row = await userModel.find({ _id: id })
//     for (let index = 0; index < row[0].Ast.length; index++) {
//         const AstElement = row[0].Ast[index];
//       //  //console.log(AstElement.toString())
//         array.push(AstElement.toString())
//         await getAltUserId(array, AstElement)
//     }
// } 


 async function getAltUserId(array, id) {
    let row = await userModel.find({ Ust: id })

    for (let index = 0; index < row.length; index++) {
        const AstElement = row[index]._id;
      //  //console.log(AstElement.toString())
        array.push(AstElement.toString())
        await getAltUserId(array, AstElement)
    }
} 
exports.getAltUserId=getAltUserId;
 
exports.getAllAstUserList = async (req, res) => {
    ////console.log(req.TokenUser);
    var array = [];
    //array.push(req.TokenUser.id)
    await getAltUserId(array, req.TokenUser.id)
    //console.log(array)
    return req.returnTemplate(array, "", 200)
};

/**
 * Admin kullanicinin su bilgilerini degistirebilir:
 *      - active
 *      - role    
 * 
 */
exports.updateUserByAdmin = async (req, res) => {
    var updateValues = {}

    if (req.body.user.active) {
        updateValues['active'] = req.body.user.active
    }
    if (req.body.user.role) {
        updateValues['role'] = req.body.user.role
    }
    var user = await userModel.findById(req.body.user.id)
    if (user) {
        user.role = req.body.user.role
        user.active = req.body.user.active
    }
    user.save((err, u) => {
        if (err) {
            return req.returnTemplate([], err, 500);
        }
        return req.returnTemplate(u, "")
    })
};
