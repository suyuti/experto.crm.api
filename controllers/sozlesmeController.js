const sozlesmeModel = require("../models/sozlesmeModel");

exports.list = async (req, res) => {
  let row = await sozlesmeModel.find()
  .populate("Teklif")
  // sozlesmeModel.find({}, (err, row) => {
  //   if (err) {
  //     return req.returnTemplate([],"hata meydana geldi",500) 
  //   }
  //   return req.returnTemplate(row,"") 
  // });
  return req.returnTemplate(row, "",200)
};

exports.create = async (req, res) => {
  let payload = req.body;
  var new_sozlesmeModel = new sozlesmeModel(payload);
  let row = await new_sozlesmeModel.save()
  //  new_sozlesmeModel.save((err, row) => {
  //     if (err) {
  //       res.status(500).send(err);
  //     }
  //     res.status(201).json(row);
  //   });  
  return req.returnTemplate(row, "")
};

exports.getById = async (req, res) => {
  let row = await newsozlesmeModel.save()
  // sozlesmeModel.find({ _id: req.params.id }, (err, row) => {
  //   if (err) {
  //     res.status(500).send(err);
  //   }
  //   res.status(200).json(row);
  // });
  return req.returnTemplate(row, "")
};

exports.update = async (req, res) => {
  sozlesmeModel.findOneAndUpdate(
    { _id: req.params.id },
    req.body,
    { new: true },
    (err, row) => {
      if (err) {
        return req.returnTemplate([], err,500);
      }
      return req.returnTemplate(row, "")
    }
  );
};

exports.delete = async (req, res) => {
  sozlesmeModel.remove({ _id: req.params.id }, (err, Category) => {
    if (err) { 
      return req.returnTemplate([], err , 404);
    }
    return req.returnTemplate([], "başarı ile silinmiştir") 
  });
};
