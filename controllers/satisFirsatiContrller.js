const satisFirsatiModel = require("../models/satisFirsatiModel");
const gorusmeKayitlariModel = require("../models/gorusmeKayitlariModel");

const randevuModel = require("../models/randevuModel");
const teklifModel = require("../models/teklifModel");
const kisiModel = require("../models/kisiModel");
exports.list = async (req, res) => {
  if (req.query.id) {
    let row = await satisFirsatiModel.findOne({ _id: req.query.id })
      .populate("Firma")
      .populate([{
        path: 'GorusmeKayitlari',
        populate: {
          path: 'Kim'
        }
      }, {
        path: 'GorusmeKayitlari',
        populate: {
          path: 'Kiminle'
        }
      }, {
        path: 'GorusmeKayitlari',
        populate: {
          path: 'Musteri'
        }
      }, {
        path: 'Randevu',
        populate: {
          path: 'Kim'
        }
      }, {
        path: 'Randevu',
        populate: {
          path: 'Kiminle'
        }
      }, {
        path: 'Randevu',
        populate: {
          path: 'Musteri'
        }
      }, {
        path: 'Teklifler',
        populate: {
          path: 'Firma'
        }
      }, {
        path: 'Teklifler',
        populate: {
          path: 'MusteriTemsilcisi'
        }
      }, {
        path: 'Teklifler',
        populate: {
          path: 'Urun'
        }
      }])
    return req.returnTemplate(row, "", 200)
  }
  let row = await satisFirsatiModel.find()
    .populate("Firma")
    .populate("GorusmeKayitlari")
    .populate("Randevu")
    .populate("Teklifler")
  return req.returnTemplate(row, "", 200)
};




exports.create = async (req, res) => {
  //   {
  //     "Firma"                   : "5e0dc96159451e6b40d283cb",
  //    "GorusmeKayitlari"        : [],
  //    "Randevu"                 : [],
  //    "Teklifler"               : [],
  //    "State"                   : true , 
  //    "Durumlar"                : {
  //        "Gorusme" :  false  ,
  //        "Randevu" :  false  ,
  //        "Teklif"  :  false  
  //    }
  //  }
  let payload = req.body;
  var new_satisFirsatiModel = new satisFirsatiModel(payload);
  let row = await new_satisFirsatiModel.save()

  row = await satisFirsatiModel.findById(row._id)
    .populate("Firma")
    .populate("GorusmeKayitlari")
    .populate("Randevu")
    .populate("Teklifler")

  return req.returnTemplate(row, "")
};

exports.addGorusmeKayitlari = async (req, res) => {
  // {
  //   "Musteri"  : "5e4150578083ae59c03c60d3",
  //    "Kanal"  : "kanal1",
  //    "Tarih"  :"Mon Feb 10 2020 17:08:44 GMT+0300 (GMT+03:00)",
  //    "Kiminle"  : "5e31464b075f4235b3044f51",
  //    "Kim"  :  "5e15c066ecc56c06e0ae9167",
  //    "Not"  : [] 
  //  }
  let payload = req.body;
  var satisFirsati = await satisFirsatiModel.findById(req.params.id)
  payload["Musteri"] = satisFirsati.Firma

  var new_gorusmeKayitlariModel = new gorusmeKayitlariModel(payload);
  let rowGorusmeKayitlari = await new_gorusmeKayitlariModel.save()
  satisFirsatiModel.findOneAndUpdate({ _id: req.params.id }, { 'Durumlar.Gorusme': true, $push: { GorusmeKayitlari: rowGorusmeKayitlari._id } }, (err, row) => {
    if (err) {
      return req.returnTemplate([], err, 500);
    }
    // row.GorusmeKayitlari.push(rowGorusmeKayitlari._id)
    // row.Durumlar.Gorusme = true;
    req.query.id = req.params.id
    exports.list(req, res)

    // return req.returnTemplate(row, "")
  });
};

exports.addRandevu = async (req, res) => {
  let payload = req.body;
  var satisFirsati = await satisFirsatiModel.findById(req.params.id)
  payload["Musteri"] = satisFirsati.Firma
  var new_randevuModel = new randevuModel(payload);
  let rowRandevu = await new_randevuModel.save();
  satisFirsatiModel.findOneAndUpdate({ _id: req.params.id }, { 'Durumlar.Randevu': true, $push: { Randevu: rowRandevu._id } }, (err, row) => {
    if (err) {
      return req.returnTemplate([], err, 500);
    }
    req.query.id = req.params.id
    exports.list(req, res)
    // row.GorusmeKayitlari.push(rowRandevu._id)
    // row.Durumlar.Randevu = true;
    // return req.returnTemplate(row, "")
  });
};

exports.addTeklif = async (req, res) => {
  let payload = req.body;
  var satisFirsati = await satisFirsatiModel.findById(req.params.id)
  payload["Firma"] = satisFirsati.Firma;
  if (satisFirsati.KapanisTarihi) {
    var date = new Date(satisFirsati.VerilisTarihi);
    date.setDate(date.getDate() + 15)
    payload["KapanisTarihi"] = date;
  }

  var new_teklifModel = new teklifModel(payload);
  let rowTeklif = await new_teklifModel.save();
  let teklif = await teklifModel.findById(rowTeklif._id).populate('Firma')
  satisFirsatiModel.findOneAndUpdate({ _id: req.params.id }, { 'Durumlar.Teklif': true, $push: { Teklifler: rowTeklif._id } }, (err, row) => {
    if (err) {
      return req.returnTemplate([], err, 500);
    }
    // row.Teklifler.push(rowTeklif._id)
    // row.Durumlar.Teklif = true;



    return req.returnTemplate(teklif, "")
  });
};









exports.getById = async (req, res) => {
  let row = await satisFirsatiModel.find({ _id: req.params.id });
  // satisFirsatiModel.find({ _id: req.params.id }, (err, row) => {
  //   if (err) {
  //     res.status(500).send(err);
  //   }
  //   res.status(200).json(row);
  // });
  return req.returnTemplate(row, "")
};

exports.update = async (req, res) => {
  satisFirsatiModel.findOneAndUpdate(
    { _id: req.params.id },
    req.body,
    { new: true },
    (err, row) => {
      if (err) {
        return req.returnTemplate([], err, 500);
      }
      return req.returnTemplate(row, "")
    }
  );
};

exports.delete = async (req, res) => {
  satisFirsatiModel.remove({ _id: req.params.id }, (err, Category) => {
    if (err) {
      return req.returnTemplate([], err, 404);
    }
    return req.returnTemplate([], "başarı ile silinmiştir")
  });
};




exports.getSatisFirsatiFirmasininKisileri = async (req, res) => {
  let row = await satisFirsatiModel.findById(req.params.id)
    .populate("Firma")
    .populate("GorusmeKayitlari")
    .populate("Randevu")
    .populate("Teklifler")

  row = await kisiModel.find({ Firma: row.Firma._id });

  return req.returnTemplate(row, "")
};

