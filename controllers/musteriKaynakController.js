const musteriKaynakModel = require("../models/musteriKaynakModel");

exports.list = async (req, res) => {
  if (req.query.id) {
    let row = await musteriKaynakModel.findOne({ _id: req.query.id })
    return req.returnTemplate(row, "", 200)
  }
  let row = await musteriKaynakModel.find()

  // musteriKaynakModel.find({}, (err, row) => {
  //   if (err) {
  //     return req.returnTemplate([],"hata meydana geldi",500) 
  //   }
  //   return req.returnTemplate(row,"") 
  // });
  return req.returnTemplate(row, "", 200)
};

exports.create = async (req, res) => {
  let payload = req.body;
  var new_musteriKaynakModel = new musteriKaynakModel(payload);
  let row = await new_musteriKaynakModel.save()
  //  new_musteriKaynakModel.save((err, row) => {
  //     if (err) {
  //       res.status(500).send(err);
  //     }
  //     res.status(201).json(row);
  //   });  
  return req.returnTemplate(row, "")
};

exports.getById = async (req, res) => {
  let row = await newmusteriKaynakModel.save()
  // musteriKaynakModel.find({ _id: req.params.id }, (err, row) => {
  //   if (err) {
  //     res.status(500).send(err);
  //   }
  //   res.status(200).json(row);
  // });
  return req.returnTemplate(row, "")
};

exports.update = async (req, res) => {
  musteriKaynakModel.findOneAndUpdate(
    { _id: req.params.id },
    req.body,
    { new: true },
    (err, row) => {
      if (err) {
        return req.returnTemplate([], err, 500);
      }
      return req.returnTemplate(row, "")
    }
  );
};

exports.delete = async (req, res) => {
  musteriKaynakModel.remove({ _id: req.params.id }, (err, Category) => {
    if (err) {
      return req.returnTemplate([], err, 404);
    }
    return req.returnTemplate([], "başarı ile silinmiştir")
  });
};
