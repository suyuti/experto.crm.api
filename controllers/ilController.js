const ilModel = require("../models/ilModel");
const ilceModel = require("../models/ilceModel");

exports.list = async (req, res) => {
  let row = await ilModel.find() 
  return req.returnTemplate(row, "",200)
};

exports.ilce = async (req, res) => {
  var ilId=req.params.id
  let row = await ilceModel.find({IlCode:ilId}) 
  return req.returnTemplate(row, "",200)
};

// exports.create = async (req, res) => {
//   let payload = req.body;
//   var new_ilModel = new ilModel(payload);
//   let row = await new_ilModel.save() 
//   return req.returnTemplate(row, "")
// };

// exports.getById = async (req, res) => {
//   let row = await ilModel.find({ _id: req.params.id } )
//   return req.returnTemplate(row, "")
// };

// exports.update = async (req, res) => {
//   ilModel.findOneAndUpdate(
//     { _id: req.params.id },
//     req.body,
//     { new: true },
//     (err, row) => {
//       if (err) {
//         return req.returnTemplate([], err,500);
//       }
//       return req.returnTemplate(row, "")
//     }
//   );
// };

// exports.delete = async (req, res) => {
//   ilModel.remove({ _id: req.params.id }, (err, Category) => {
//     if (err) { 
//       return req.returnTemplate([], err , 404);
//     }
//     return req.returnTemplate([], "başarı ile silinmiştir") 
//   });
// };
