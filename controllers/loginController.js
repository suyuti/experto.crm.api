const userModel = require("../models/userModel");
const command = require("../command");
const httpResult = require('../config');
var ejs = require('ejs');


exports.login = async (req, res) => {
    let payload = req.body;
    let user = await userModel.find({ KullaniciAdi: payload.KullaniciAdi });
    if (user.length == 0) {
        return req.returnTemplate([], "Kullanıcı bulunamadı", httpResult.error);
    }
    user = user[0];
    let veryfy = command.VerifyHash(payload.Password, user.Password);
    //console.log(veryfy);
    if (veryfy) {

        var tokenData = {
            _id: user._id,
            KullaniciAdi: user.KullaniciAdi,
        }
        const token = command.getToken(tokenData);
        tokenData["Token"] = token;

        tokenData["Adi"] = user.Adi;
        tokenData["Email"] = user.Email;
        tokenData["Ust"] = user.Ust;
        tokenData["Ast"] = user.Ast;

        return req.returnTemplate(tokenData, "")
    } else {
        return req.returnTemplate([], "Bilgileriniz yanlış lütfen kontrol edin", httpResult.error)
    }
};


/// test için string ifadeyi hashlanmiş bir şekile cevirecek
exports.createHash = async (req, res) => {
    let key = req.params.key;
    const keyHash = command.HashPassword(key);
    var sendData = {
        key: key,
        keyHash: keyHash
    }

    return req.returnTemplate(sendData, "Deneme amaçlıdır. kayıt yapılmaz", httpResult.error)

};



exports.forgotPassword = async (req, res) => {
    let payload = req.body;
    let user = await userModel.find({ Email: payload.Email });
    if (user.length == 0) {
        return req.returnTemplate([], "Kullanıcı Sistemde Kayıtlı Degil Lütfen Kontrol Ediniz", httpResult.error);
    }
    user = user[0];
    var forgotData = {
        _id: user._id,
        Email: user.Email,
        Date: new Date()
    }
    const forgotPasswordKey = command.getToken(forgotData); 
    try {
        const html = await ejs.renderFile('./Views/forgotPassword.ejs', {
            title: '[FORGOT_PASSWORD]',
            data: user,
            forgotPasswordKey: forgotPasswordKey
        })
       
        command.sendMail(user.Email, '[FORGOT_PASSWORD]', html);
        return req.returnTemplate([], "Şifre Degiştirmek için forgot Password Key Mailinize Gönderilmiştir", httpResult.success)
    } catch (error) {
        return req.returnTemplate([], "Mail Gönderilemedi", httpResult.error)

    }
};
exports.forgotPasswordChange = async (req, res) => {
    let payload = req.body;
    let nowDate = new Date();


    if (payload.forgotPasswordKey) {
        var decoded = command.jwtDecode(payload.forgotPasswordKey)
        try {
            decoded.Date = new Date(decoded.Date);
            decoded.Date.setUTCHours(decoded.Date.getUTCHours() + 12);//12 saat gecerli

            if (nowDate < decoded.Date) {
                if (!payload["newPassword"]) {
                    return req.returnTemplate([], "Lütfen Şifre Giriniz", httpResult.success);
                } else {

                    await userModel.findOneAndUpdate(
                        { _id: decoded._id },
                        { Password: command.HashPassword(payload["newPassword"]) },
                        { new: true },
                        (err, row) => {
                            if (err) {
                                return req.returnTemplate([], err, httpResult.error);
                            }
                            return req.returnTemplate([], "Şifreniz Güncellenmiştir", httpResult.success);
                        }
                    );

                    //  return req.returnTemplate(decoded, "decoded", httpResult.success);
                }


            } else {
                return req.returnTemplate([], "forgot Password Key Geçerlilik Süresi Dolmuştur.Lütfen Linki Yenileyin.", httpResult.error);
            }
        } catch (error) {

            return req.returnTemplate([], error, httpResult.error);
        }



        //  return req.returnTemplate(command.jwtDecode(payload.forgotPasswordKey), "decoded", httpResult.success);

    } else {
        return req.returnTemplate([], "forgot Password Key YOK Lütfen Tekrar Deneyiniz", httpResult.error);
    }


};



