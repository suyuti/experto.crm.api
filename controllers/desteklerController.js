const desteklerModel = require("../models/desteklerModel");

exports.list = async (req, res) => {
  if (req.query.id) {
    let row = await desteklerModel.findOne({ _id: req.query.id })
    return req.returnTemplate(row, "", 200)
  }
  let row = await desteklerModel.find() 
  // desteklerModel.find({}, (err, row) => {
  //   if (err) {
  //     return req.returnTemplate([],"hata meydana geldi",500) 
  //   }
  //   return req.returnTemplate(row,"") 
  // });
  return req.returnTemplate(row, "", 200)
};

exports.create = async (req, res) => {
  let payload = req.body;
  var new_desteklerModel = new desteklerModel(payload);
  let row = await new_desteklerModel.save()
  //  new_desteklerModel.save((err, row) => {
  //     if (err) {
  //       res.status(500).send(err);
  //     }
  //     res.status(201).json(row);
  //   });  
  return req.returnTemplate(row, "")
};

 

exports.update = async (req, res) => {
  desteklerModel.findOneAndUpdate(
    { _id: req.params.id },
    req.body,
    { new: true },
    (err, row) => {
      if (err) {
        return req.returnTemplate([], err, 500);
      }
      return req.returnTemplate(row, "")
    }
  );
};

exports.delete = async (req, res) => {
  desteklerModel.remove({ _id: req.params.id }, (err, Category) => {
    if (err) {
      return req.returnTemplate([], err, 404);
    }
    return req.returnTemplate([], "başarı ile silinmiştir")
  });
};
