const OSBModel = require("../models/OSBModel");

exports.list = async (req, res) => {
  let row = await OSBModel.find()
  // OSBModel.find({}, (err, row) => {
  //   if (err) {
  //     return req.returnTemplate([],"hata meydana geldi",500) 
  //   }
  //   return req.returnTemplate(row,"") 
  // });
  return req.returnTemplate(row, "",200)
};

exports.create = async (req, res) => {
  let payload = req.body;
  var new_OSBModel = new OSBModel(payload);
  let row = await new_OSBModel.save()
  //  new_OSBModel.save((err, row) => {
  //     if (err) {
  //       res.status(500).send(err);
  //     }
  //     res.status(201).json(row);
  //   });  
  return req.returnTemplate(row, "")
};

exports.getById = async (req, res) => {
  let row = await OSBModel.find({ _id: req.params.id });
  // OSBModel.find({ _id: req.params.id }, (err, row) => {
  //   if (err) {
  //     res.status(500).send(err);
  //   }
  //   res.status(200).json(row);
  // });
  return req.returnTemplate(row, "")
};

exports.update = async (req, res) => {
  OSBModel.findOneAndUpdate(
    { _id: req.params.id },
    req.body,
    { new: true },
    (err, row) => {
      if (err) {
        return req.returnTemplate([], err,500);
      }
      return req.returnTemplate(row, "")
    }
  );
};

exports.delete = async (req, res) => {
  OSBModel.remove({ _id: req.params.id }, (err, Category) => {
    if (err) { 
      return req.returnTemplate([], err , 404);
    }
    return req.returnTemplate([], "başarı ile silinmiştir") 
  });
};
