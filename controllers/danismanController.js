const danismanModel = require("../models/danismanModel");

exports.list = async (req, res) => {
  if (req.query.id) {
    let row = await danismanModel.findOne({ _id: req.query.id })
    return req.returnTemplate(row, "", 200)
  }
  let row = await danismanModel.find()


  // danismanModel.find({}, (err, row) => {
  //   if (err) {
  //     return req.returnTemplate([],"hata meydana geldi",500) 
  //   }
  //   return req.returnTemplate(row,"") 
  // });
  return req.returnTemplate(row, "", 200)
};

exports.create = async (req, res) => {
  let payload = req.body;
  var new_danismanModel = new danismanModel(payload);
  let row = await new_danismanModel.save()
  //  new_danismanModel.save((err, row) => {
  //     if (err) {
  //       res.status(500).send(err);
  //     }
  //     res.status(201).json(row);
  //   });  
  return req.returnTemplate(row, "")
};



exports.update = async (req, res) => {
  danismanModel.findOneAndUpdate(
    { _id: req.params.id },
    req.body,
    { new: true },
    (err, row) => {
      if (err) {
        return req.returnTemplate([], err, 500);
      }
      return req.returnTemplate(row, "")
    }
  );
};

exports.delete = async (req, res) => {
  danismanModel.remove({ _id: req.params.id }, (err, Category) => {
    if (err) {
      return req.returnTemplate([], err, 404);
    }
    return req.returnTemplate([], "başarı ile silinmiştir")
  });
};
