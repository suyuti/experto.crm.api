const gorusmeKayitlariModel = require("../models/gorusmeKayitlariModel");

exports.list = async (req, res) => {
  if (req.query.id) {
    let row = await gorusmeKayitlariModel.findOne({ _id: req.query.id })
      .populate('Teklif')
      .populate('Kiminle')
      .populate('Kim')
    return req.returnTemplate(row, "", 200)
  }
  let row = await gorusmeKayitlariModel.find()
    .populate('Teklif')
    .populate('Kiminle')
    .populate('Kim')
  // gorusmeKayitlariModel.find({}, (err, row) => {
  //   if (err) {
  //     return req.returnTemplate([],"hata meydana geldi",500) 
  //   }
  //   return req.returnTemplate(row,"") 
  // });
  return req.returnTemplate(row, "", 200)
};

exports.create = async (req, res) => {
  let payload = req.body;
  var new_gorusmeKayitlariModel = new gorusmeKayitlariModel(payload);
  let row = await new_gorusmeKayitlariModel.save()
  //  new_gorusmeKayitlariModel.save((err, row) => {
  //     if (err) {
  //       res.status(500).send(err);
  //     }
  //     res.status(201).json(row);
  //   });  
  return req.returnTemplate(row, "")
};

 

exports.update = async (req, res) => {
  gorusmeKayitlariModel.findOneAndUpdate(
    { _id: req.params.id },
    req.body,
    { new: true },
    (err, row) => {
      if (err) {
        return req.returnTemplate([], err, 500);
      }
      return req.returnTemplate(row, "")
    }
  );
};

exports.delete = async (req, res) => {
  gorusmeKayitlariModel.remove({ _id: req.params.id }, (err, Category) => {
    if (err) {
      return req.returnTemplate([], err, 404);
    }
    return req.returnTemplate([], "başarı ile silinmiştir")
  });
};
