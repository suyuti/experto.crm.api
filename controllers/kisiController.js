const kisiModel = require("../models/kisiModel");

exports.list = async (req, res) => {
  if (req.query.id) {
    let row = await kisiModel.findOne({ _id: req.query.id }).populate('Firma')
    return req.returnTemplate(row, "",200)
  } 
     let row = await kisiModel.find().populate('Firma')
  
  // kisiModel.find({}, (err, row) => {
  //   if (err) {
  //     return req.returnTemplate([],"hata meydana geldi",500) 
  //   }
  //   return req.returnTemplate(row,"") 
  // });
  return req.returnTemplate(row, "",200)
};

exports.create = async (req, res) => {
  let payload = req.body;
  //console.log(payload)
  var new_kisiModel = new kisiModel(payload);
  //console.log(new_kisiModel)
  let row = await new_kisiModel.save()
  //  new_kisiModel.save((err, row) => {
  //     if (err) {
  //       res.status(500).send(err);
  //     }
  //     res.status(201).json(row);
  //   });  
  return req.returnTemplate(row, "")
};
 

exports.update = async (req, res) => {
  kisiModel.findOneAndUpdate(
    { _id: req.params.id },
    req.body,
    { new: true },
    (err, row) => {
      if (err) {
        return req.returnTemplate([], err,500);
      }
      return req.returnTemplate(row, "")
    }
  );
};

exports.delete = async (req, res) => {
  kisiModel.remove({ _id: req.params.id }, (err, Category) => {
    if (err) { 
      return req.returnTemplate([], err , 404);
    }
    return req.returnTemplate([], "başarı ile silinmiştir") 
  });
};
