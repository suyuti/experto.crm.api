var _ = require('lodash');
const jwt = require('jsonwebtoken');
const config = require('../config');
const userModel = require("../models/userModel");
const command = require("../command");


exports.auth = async (req, res) => {
    const data = req.body.data;
    const {email, password} = data;
    const _user = await userModel.findOne( {KullaniciAdi : email, active: true})    
    
    const user = _.cloneDeep(_user);
    let verified =false
    user ? verified= command.VerifyHash(password, user.Password):"";

    const error = {
        email   : user ? null : 'Check your username/email',
        password: user && verified ? null : 'Check your password'
    };

    if ( !error.email && !error.password && !error.displayName )
    {
        delete user['Password'];

        const access_token = jwt.sign({id: user.id}, config.secret, {expiresIn: config.expiresIn});

        const response = {
            "user"        : user,
            "access_token": access_token
        };

        return res.status(200).json(response);
    }
    else
    {
        return res.status(200).json(error);
    }
}


exports.access_token = async (req, res) => {
    const data = req.body.data;
    const {access_token} = data;

    try
    {
        const {id} = jwt.verify(access_token, config.secret);
        var _user = await userModel.findById(id)
        if(!_user){
            const errorUser = "User not Found Or not Activeted";
            return res.status(200).json(error)
        }
       
        const user = _.cloneDeep(_user);
        delete user['password'];

        const updatedAccessToken = jwt.sign({id: user.id}, config.secret, {expiresIn: config.expiresIn});

        const response = {
            "user"        : user,
            "access_token": updatedAccessToken
        };

        return res.status(200).json(response)
    } catch ( e )
    {
        const error = "Invalid access token detected";
        return res.status(200).json(error)
    }
}

exports.register = async (req, res) => {
    const data = req.body
    const {displayName, password, email} = data;
    const isEmailExists = await userModel.findOne({"data.email" : email});
    const error = {
        email      : isEmailExists ? 'The email is already in use' : null,
        displayName: displayName !== '' ? null : 'Enter display name',
        password   : null
    };
    if ( !error.displayName && !error.password && !error.email )
    {

        var pass = command.HashPassword(password)
        var newUser = new userModel({
            //uuid    : 1,//FuseUtils.generateGUID(),
            //from    : 'custom-db',
            Adi: displayName,
            KullaniciAdi: email,
            Email: email,
            Password: pass,
            role    : "admin",
            data    : {
                displayName: displayName,
                photoURL   : 'assets/images/avatars/Abbott.jpg',
                email      : email,
                settings   : {},
                shortcuts  : []
            }
        })
        var _newUser = await newUser.save()
        //authDB.users = [
        //    ...authDB.users,
        //    newUser
        //];

        const user = _.cloneDeep(_newUser);
        delete user['password'];

        const access_token = jwt.sign({id: user.id}, config.secret, {expiresIn: config.expiresIn});

        const response = {
            "user"        : user,
            "access_token": access_token
        };

        return res.status(200).json(response);
    }
    else
    {
        return res.status(200).json(error);
    }
}
exports.update = async (req, res) => {}
