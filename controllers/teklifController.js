const teklifModel = require("../models/teklifModel");
var   ejs         = require('ejs');
const command     = require('../command')

exports.list = async (req, res) => {
   if (req.query.teklifId) {
      let row = await teklifModel.findOne({_id: req.query.teklifId})
         .populate({
            path: 'MusteriTemsilcisi',
            populate: {
               path: 'Ust'
            }
         })
         .populate("Urun")
         .populate("Firma")
         .populate("IlgiliKisi")
      return req.returnTemplate(row, "", 200)
   }
   else {
      let row = await teklifModel.find()
         .populate({
            path: 'MusteriTemsilcisi',
            populate: {
               path: 'Ust'
            }
         })
         .populate("Urun")
         .populate("Firma")
         .populate("IlgiliKisi")
      return req.returnTemplate(row, "", 200)
   }
};

exports.create = async (req, res) => {
   let payload = req.body;
   var new_teklifModel = new teklifModel(payload);
   let saved = await new_teklifModel.save()
   let row = await teklifModel.findById(saved._id).populate('Firma')
   return req.returnTemplate(row, "")
};

exports.getById = async (req, res) => {
   let row = await teklifModel.find({ _id: req.params.id })
   return req.returnTemplate(row, "")
};

exports.update = async (req, res) => {
   let payload = req.body;
   let id = payload._id;
   delete payload._id;
   teklifModel.update({ _id: id }, payload, { upsert: true }, (err, row) => {
      if (err) { return req.returnTemplate([], err, 500); }
      teklifLog(id, `${req.TokenUser._id} li kişi tarafından guncellenmiştir`);
      return req.returnTemplate(row, "")
   });
};

exports.delete = async (req, res) => {
   teklifModel.remove({ _id: req.params.id }, (err, Category) => {
      if (err) {
         return req.returnTemplate([], err, 404);
      }
      teklifLog(req.params.id, `${req.TokenUser._id} li kişi tarafından silinmiştir`);
      return req.returnTemplate([], "başarı ile silinmiştir")
   });
};

exports.onayiste = async (req, res) => {
   let id = req.params.id
   var update = { "TeklifDurumu": 1 }
   let row = await teklifModel.find({ _id: id })
      .populate({
         path: 'MusteriTemsilcisi',
         populate: {
            path: 'Ust'
         }
      })
      .populate("Urun")
      .populate("Firma")
      .populate("IlgiliKisi")

   const html = await ejs.renderFile('./Views/teklifOnayTalebi.ejs', {
      title: '[TEKLIF_ONAY_TALEBI]',
      data: row[0]
   })
   var to = row[0].MusteriTemsilcisi.Ust.Email
   var subject = "[TEKLIF_ONAY_TALEBI]"
   try {
      command.sendMail(to, subject, html); 
      teklifDurumuGuncelle(req, res, id, update);
   } catch (error) {
      //console.log(error);
      return req.returnTemplate(error, "", 500)
   }

};


exports.onayVer = async (req, res) => {
   let id = req.params.id
   var update = { "TeklifDurumu": 10, Onaylayan: req.TokenUser._id }
   teklifDurumuGuncelle(req, res, id, update);
};

exports.state = async (req, res) => {
   let id = req.params.id
   let state = req.params.state
   var update = { "TeklifDurumu": state }
   teklifDurumuGuncelle(req, res, id, update);
};


function teklifDurumuGuncelle(req, res, id, update) {
   teklifModel.updateOne({ _id: id }, update, { upsert: true }, (err, row) => {
      if (err) {
         return req.returnTemplate([], err, 500);
      }
      teklifLog(id, `${req.TokenUser._id} li kişi teklifin durumunu ${durum} olarak guncelledi`)
      return req.returnTemplate(row, "")
   });
}


function teklifLog(id, text) {
   var log = {
      date: new Date(),
      description: text
   }
   teklifModel.findOneAndUpdate({ _id: id }, { $push: { Loglar: log } }, (err, row) => {
      if (err) {
         //console.log(err)
      }
      //console.log(log)
   });
}

exports.teklifLog = teklifLog;
