const mongoose = require("mongoose");
 mongoose.set('useCreateIndex', true);

//const dbURI ="mongodb://127.0.0.1:27017/expertocrm";
var user    = process.env.DB_USER;
var pass    = process.env.DB_PASS;
var db      = process.env.DB_NAME;
var domain  = process.env.DB_DOMAIN;
var port    = process.env.DB_PORT;
 
const dbURI = `mongodb://${user}:${pass}@${domain}:${port}/${db}`

const options = {
  // reconnectTries: Number.MAX_VALUE,
  // poolSize: 10,
  // useNewUrlParser: true,
  // useUnifiedTopology: true
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false 
};

/*
exports.dbconnection= function (req, res, next) { 

  mongoose.connect(dbURI, options,function(err,db){
    if(err){
        //console.log(err);
        req.db=db;
    }
    else {
        //console.log(dbURI + "veritabanına baglanıldı");
        db.close();
        next();
    }
  })

}*/


mongoose.connect(dbURI, options).then(
  () => {
    //console.log("Veritabanına baglanıldı");
  },
  err => {
    //console.log("Veritabanı hatası: ", err);
  }
);