const mongoose = require("mongoose");
const Schema = mongoose.Schema;

  
var SchemaModel = new Schema({ 
  Adi           : { type: String,  required: true } ,
  KullaniciAdi  : { type: String,  unique : true, required : true, dropDups: true} ,
  Email         : { type: String,  required: true } ,
  Password      : { type: String,  required: true } ,
  Ust           : { type: Schema.Types.ObjectId, ref: 'user' },
  Ast           : { type: [Schema.Types.ObjectId], ref: 'kisiler' },
  IndirimOrani  : { type: Number },

  active: {type: Boolean, default: false},
  role : {type:String},
  data : {
    displayName: String,
    photoURL   : String,
    email      : String,
    settings     : {
        layout   : {
            style : {type: String, default: 'layout1'},
            config: {
                scroll : {type: String, default: 'content'},
                navbar : {
                    display : {type: Boolean, default: true},
                    folded  : {type: Boolean, default: false},
                    position: {type: String, default: 'left'},
                },
                toolbar: {
                  display : {type: Boolean, default: true},
                  style  : {type: String, default: 'fixed'},
                  position: {type: String, default: 'below'},
              },
                footer : {
                  display : {type: Boolean, default: true},
                  style  : {type: String, default: 'fixed'},
                  position: {type: String, default: 'below'},
                },
                mode   : {type: String, default: 'fullWidth'}
            }
        },
        customScrollbars: {type: Boolean, default: true},
        theme           : {
            main   : {type: String, default: 'default'},
            navbar : {type: String, default: 'default'},
            toolbar: {type: String, default: 'default'},
            footer : {type: String, default: 'default'}
        }
    },
    shortcuts    : [String]
}

},{ 
  versionKey: false 
});


module.exports = mongoose.model("user", SchemaModel);
