const mongoose = require("mongoose");
const Schema = mongoose.Schema;

  
var SchemaModel = new Schema({ 
  SozlesmeNo  : {  type: String,  required: true } ,
  Teklif  : { type: Schema.Types.ObjectId, ref: 'teklifler' },
  SozlesmeTarihi  : {  type: String,  required: true } ,
  SozlesmeSuresi  : {  type: String,  required: true } , 
  
},{ 
  versionKey: false 
});


module.exports = mongoose.model("sozlesmeler", SchemaModel);
