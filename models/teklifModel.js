const mongoose = require("mongoose");
const Schema = mongoose.Schema;

  
var SchemaModel = new Schema({ 
  TeklifNo              : { type: String },
  MusteriTemsilcisi     : { type: Schema.Types.ObjectId, ref: 'user' },
  Urun                  : { type: Schema.Types.ObjectId, ref: 'urunler' },
  Firma                 : { type: Schema.Types.ObjectId, ref: 'musteriler' }, 
  IadeGarantiOran       : { type: String } ,
  IlgiliKisi            : { type: Schema.Types.ObjectId, ref: 'kisiler' },
  KapanisTarihi         : { type: Date,  required: true } ,// default degeri VerilisTarihi 15 gun sonarası
  VerilisTarihi         : { type: Date,  default:new Date() } ,
  TeklifDurumu          : { type: Number,  default: 0 } ,
  Loglar                : [],
  OnOdemeTutari         : { type: Number } ,
  RaporBasiOdemeTutari  : { type: Number } ,
  YuzdeTutari           : { type: Number } ,
  SabitTutar            : { type: Number } ,
  BasariTutari          : { type: Number } ,
  SartlarVeKosullar     : { type: String } ,
  IadeliMi              : { type: Boolean } ,
  ProjeSure             : { type: Number } ,  
  Onaylayan             : { type: Schema.Types.ObjectId, ref: 'user' },
  AraToplam             : { type: Number },
  KDVOran               : { type: Number },
  KDV                   : { type: Number },
  Indirim               : { type: Number },
  Toplam                : { type: Number },
  Sozlesme              : { type: Schema.Types.ObjectId, ref: 'sozlesmeler' },
},{ 
  versionKey: false 
});


module.exports = mongoose.model("teklifler", SchemaModel);

// TeklifDurumu: [
//     { id: 0, name: "Yeni Teklif Oluşturuldu" },
//     { id: 1, name: "Onay bekleniyor" },  
//     { id: 10, name: "Onay verildi" },   
//     { id: 101, name: "İlk toplantı yapıldı" },
//     { id: 102, name: "Teklif Verildi" },
//     { id: 103, name: "Ön teknik toplantı yapıldı" },
//     { id: 104, name: "Sözleşme Gönderildi" },
//     { id: 105, name: "Sözleşme yapıldı " }, 
//     { id: 400, name: "Kaybedildi" }, 
//     { id: 401, name: "Teklif Silindi" },
//     { id: 402, name: "Teklif Red Edildi" },
// ],



 