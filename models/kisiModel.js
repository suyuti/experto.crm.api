const mongoose = require("mongoose");
const Schema = mongoose.Schema;

  
var SchemaModel = new Schema({ 
  Adi       : {  type: String,  required: true } ,
  Soyadi    : {  type: String,  required: true } ,
  Unvani    : {  type: String } ,
  Cinsiyet  : {  type: String } ,
  CepTel    : {  type: String } ,
  Mail      : {  type: String } ,
  IsTel     : {  type: String } ,
  Dahili    : {  type: String } ,
  Firma     : { type: mongoose.Schema.Types.ObjectId, ref: 'musteriler', required: true},
  Type      : {  type: Number } // ? 
},{ 
  versionKey: false 
});


module.exports = mongoose.model("kisiler", SchemaModel);



// type
// 1:Yönetim 
// 2:Mali
// 3:Teknik