const mongoose = require("mongoose");
const Schema = mongoose.Schema;

var SchemaModel = new Schema({  
  Name                   : { type:String, required: true},
  Firma                   : { type: mongoose.Schema.Types.ObjectId, ref: 'musteriler', required: true},
  GorusmeKayitlari        : [{ type: mongoose.Schema.Types.ObjectId, ref: 'gorusmeKayitlari', default: []}],
  Randevu                 : [{ type: mongoose.Schema.Types.ObjectId, ref: 'randevu', default: []}],
  Teklifler               : [{ type: mongoose.Schema.Types.ObjectId, ref: 'teklifler', default: []}],
  State                   : {  type: Boolean,  default : true } , 
  Olusturan               : { type: Schema.Types.ObjectId, ref: 'user' },
  Sorumlu                 : { type: Schema.Types.ObjectId, ref: 'user' }, 
  Durumlar                : {
      Gorusme :{  type: Boolean,  default : false },
      Randevu :{  type: Boolean,  default : false },
      Teklif  :{  type: Boolean,  default : false }, 
  }
},{ 
  versionKey: false 
});

module.exports = mongoose.model("satisFirsati", SchemaModel);
 