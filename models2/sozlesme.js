const mongoose = require("mongoose");
const Schema = mongoose.Schema;

var SchemaModel = new Schema({ 
    Musteri         : { type: Schema.Types.ObjectId, ref: 'Musteri', required: true },
    Urun            : { type: Schema.Types.ObjectId, ref: 'Urun', required: true },
    SozlesmeNo      : { type: String,  required: false },
    SozlesmeTarihi  : { type: Date,  required: false },
},{ 
  versionKey: false 
});

module.exports = mongoose.model("Sozlesme", SchemaModel);
