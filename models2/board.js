const mongoose = require("mongoose");
const Schema = mongoose.Schema;

var SchemaModel = new Schema({ 
    Adi             : { type: String},
    URI             : { type: String},
    Settings        : {
        Color       : { type: String},
        Subscribed  : { type: String},
        CardCoverImages: { type: Boolean, default: false},
    },

    Lists: [{
        Adi: String,
        Cards: [{type: Schema.Types.ObjectId, ref: 'SatisFirsati'}]
    }],
    Cards: [{
        Adi: String,
        Aciklama: String, 
        AttachmentCoverId: String,
        MembersIds: [String],
        LabelsIds: [String],
        Attachments: [String],
        Subscribed: Boolean,
        Checklists: [String],
        Due: Date
    }],
    Members: [{
        Adi: String,
        Avatar: String
    }],
    Labels: [{
        Adi: String,
        Klass: String
    }]
},{ 
  versionKey: false 
});

module.exports = mongoose.model("SatisBoard", SchemaModel);
