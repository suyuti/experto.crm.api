const mongoose = require("mongoose");
const Schema = mongoose.Schema;

var SchemaModel = new Schema({ 
    Musteri             : { type: Schema.Types.ObjectId, ref: 'Musteri', required: true },
    GorusmeyiYapanKisi  : { type: Schema.Types.ObjectId, ref: 'User', required: true },
    GorusmeYapilanKisi  : { type: Schema.Types.ObjectId, ref: 'Kisi', required: true },
    Zaman               : { type: Date, required: true },
    Kanal               : { type: String, enum: ['Telefon', 'Mail', 'Yüzyüze', 'Whatsapp'],  required: true, default: 'Telefon' },
    Konu                : { type: String,  required: true },
    Aciklama            : { type: String },
},{ 
  versionKey: false 
});

module.exports = mongoose.model("Gorusme", SchemaModel);
