const mongoose = require("mongoose");
const Schema = mongoose.Schema;

var SchemaModel = new Schema({ 
    Musteri         : { type: Schema.Types.ObjectId, ref: 'Musteri'},
    AdresTuru       : { type: String,  required: false },
    Adres1          : { type: String,  required: false },
    Adres2          : { type: String,  required: false },
    Ilce            : { type: String,  required: false },
    Il              : { type: String,  required: false },
    PostaKodu       : { type: String,  required: false },
},{ 
  versionKey: false 
});

module.exports = mongoose.model("Adres", SchemaModel);
