const mongoose = require("mongoose");
const Schema = mongoose.Schema;

  
var SchemaModel = new Schema({
  Musteri       : { type: Schema.Types.ObjectId, ref: 'Musteri'}, 
  Adi           : { type: String,  required: true } ,
  Tarih         : { type: Date,    required: true } ,
  Durum         : { type: String,  enum:['Basarili', 'Basarisiz', 'DevamEdiyor'], required: true } ,
},{ 
  versionKey: false 
});

module.exports = mongoose.model("Destek", SchemaModel);

 