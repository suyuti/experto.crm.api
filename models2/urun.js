const mongoose = require("mongoose");
const Schema = mongoose.Schema;

  
var SchemaModel = new Schema({ 
  Adi                       : {  type: String, required: true },
  AktifMi                   : {  type: Boolean, default: true },
  OnOdemeTutari             : {  type: Number },
  OnOdemeTutariVar          : {  type: Boolean, default: false},
  RaporBasiOdemeTutari      : {  type: Number },
  RaporBasiOdemeTutariVar   : {  type: Boolean, default: false },
  YuzdeTutari               : {  type: Number },
  YuzdeTutariVar            : {  type: Boolean, default: false },
  SabitTutar                : {  type: Number },
  SabitTutarVar             : {  type: Boolean, default: false},
  BasariTutari              : {  type: Number },
  BasariTutariVar           : {  type: Boolean, default: false},
  KDVOrani                  : {  type: Number },
},
{ 
  versionKey: false 
});


module.exports = mongoose.model("Urun", SchemaModel);


// Dashboard
// 0 default
// 1 teknik
// 2 satış
// 3 mali
// 4 yönetim
