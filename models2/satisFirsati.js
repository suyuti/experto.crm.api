const mongoose = require("mongoose");
const Schema = mongoose.Schema;

var SchemaModel = new Schema({ 
    Musteri             : { type: Schema.Types.ObjectId, ref: 'Musteri', required: true },
    //OlusturanKisi       : { type: Schema.Types.ObjectId, ref: 'User', required: true },
    OlusturmaZamani     : { type: Date, default: new Date()},
    Adi                 : { type: String,  required: true },
    Aciklama            : { type: String },
},{ 
  versionKey: false 
});

module.exports = mongoose.model("SatisFirsati", SchemaModel);
