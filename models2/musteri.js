const mongoose = require("mongoose");
const Schema = mongoose.Schema;

  
var SchemaModel = new Schema({
  Unvani                : { type: String, required: false },
  Markasi               : { type: String, required: true },
  MusteriTemsilcisi     : { type: Schema.Types.ObjectId, ref: 'user'},
  MusteriKaynagi        : { type: Schema.Types.ObjectId, ref: 'MusteriKaynak'},
  Sektoru               : { type: Schema.Types.ObjectId, ref: 'Sektor'},
  Adresler              : [{ type: Schema.Types.ObjectId, ref: 'Adres'}],
  OSB                   : { type: String, required: false },
  Telefonlar            : [{ type: String, required: false }],
  WebSitesi             : { type: String, required: false },
    CalisanSayisi         : { type: Number, required: false, default: 0 },
    TeknikPersonelSayisi  : { type: Number, required: false, default: 0 },
    MuhendisSayisi        : { type: Number, required: false , default: 0},
    Ciro                  : { type: Number, required: false, default: 0 },
  KobiMi                : { type: Boolean, required: false },
  FaydalandigiDestekler : [{ type: Schema.Types.ObjectId, ref: 'Destek'}],
  Danisman              : { type: String, required: false },
  FirmaData             : {
    VergiDairesi    : { type: String, required: false },
    VergiNo         : { type: String, required: false },
    EFaturaMi       : { type: Boolean, required: false },
    TeknikIletisim  : {},
    MaliIletisim    : {},
    YonetimIletisim : {}
  },
  Active                : { type: Boolean, default: true},
  MusteriStatus         : { type: String, enum: ['aktif', 'pasif', 'potansiyel', 'sorunlu'], default: 'potansiyel'} 
},{ 
  versionKey: false 
});


module.exports = mongoose.model("Musteri", SchemaModel);
