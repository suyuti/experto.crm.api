const Sektor = require("../models2/sektor");

exports.list = async (req, res) => {
    const sektorler = await Sektor.find()
    return res.status(200).json(sektorler)
}
