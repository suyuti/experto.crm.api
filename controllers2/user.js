const User = require("../models2/user");

exports.list = async (req, res) => {
    const users = await User.find()
    return res.status(200).json(users)
}

exports.create = async (req, res) => {
    let payload = req.body;
    var user = new User(payload)
    let row = await user.save()
    return res.status(200).json(row)
}

exports.getById = async (req, res) => {
    const users = await User.find({_id: req.params.id})
    return res.status(200).json(users)
}

exports.update = async (req, res) => {
    User.findByIdAndUpdate(req.query.id, req.body, {new:true},
        (err, doc) => {
            if (err) {
                return res.status(404).json(null)
            }
            else {
                return res.status(200).json(doc)
            }
        })
}

exports.remove = async (req, res) => {
    User.remove({_id: req.query.id}, (e) => {
        if (e) {
            return res.status(404).json(err)
        }
        else {
            return res.status(200).json('')
        }
    })
}