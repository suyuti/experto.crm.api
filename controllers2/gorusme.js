const Gorusme = require("../models2/gorusme");

exports.list = async (req, res) => {
    console.log('get gorusme list')
    var ObjectId = require('mongoose').Types.ObjectId; 
    const gorusmeler = await Gorusme.find({Musteri: new ObjectId(req.params.musteriId)})
    return res.status(200).json(gorusmeler)
}
  
exports.create = async (req, res) => {
    let payload = req.body;
    var gorusme = new Gorusme(payload)
    let row = await gorusme.save()
    return res.status(200).json(row)
}

exports.getById = async (req, res) => {
    console.log('gorusme get by id')
    const gorusme = await Gorusme.find({_id: req.params.gid})
    return res.status(200).json(gorusme[0])
}

exports.remove = async (req, res) => {
    console.log('gorusme remove')
    Gorusme.remove({_id: req.params.gid}, (e) => {
        if (e) {
            return res.status(404).json(err)
        }
        else {
            return res.status(200).json('')
        }
    })
}
