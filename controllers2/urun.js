const Urun = require("../models2/urun");

exports.list = async (req, res) => {
    console.log('get urun list')
    const urunler = await Urun.find()
    return res.status(200).json(urunler)
}
  
exports.create = async (req, res) => {
    console.log('create urun')
    let payload = req.body;
    var urun = new Urun(payload)
    let row = await urun.save()
    return res.status(200).json(row)
}

exports.getById = async (req, res) => {
    console.log('get urun by id')
    const urun = await Urun.find({_id: req.params.id})
    return res.status(200).json(urun[0])
}

exports.update = async (req, res) => {
    console.log('urun update')
    Urun.findByIdAndUpdate(req.params.id, req.body, {new:true},
        (err, doc) => {
            if (err) {
                return res.status(404).json(null)
            }
            else {
                return res.status(200).json(doc)
            }
        })
}

exports.remove = async (req, res) => {
    console.log('urun sil')
    Urun.findOneAndDelete({_id: req.params.id}, (e) => {
        if (e) {
            return res.status(404).json(err)
        }
        else {
            return res.status(200).json('')
        }
    })
}

