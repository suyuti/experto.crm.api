const MusteriKaynak = require("../models2/musterikaynak");

exports.list = async (req, res) => {
    const musteriKaynaklari = await MusteriKaynak.find()
    return res.status(200).json(musteriKaynaklari)
}
