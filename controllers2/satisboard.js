const SatisBoard = require("../models2/board");

exports.list = async (req, res) => {
    console.log('Board list')
    const boards = await SatisBoard.find()
    return res.status(200).json(boards)
}

exports.create = async (req, res) => {
    console.log('Board create')
    var boardInfo = req.body.board
    console.log(boardInfo)
    var board = new SatisBoard({
        Adi: boardInfo.name,
        URI: boardInfo.uri,

    })
    let row = await board.save()

    return res.status(200).json(row)
}

exports.getById = async (req, res) => {
    console.log('Board get by id')
    console.log(req.query)
    const board = await SatisBoard.findOne({URI: req.query.URI})
    return res.status(200).json(board)
}

exports.getSatisBoard = async (req, res) => {
    console.log('get Satis Board')
    console.log(req.query)

    const board = await SatisBoard.findOne({_id: req.query.boardId})
    return res.status(200).json(board)
}
