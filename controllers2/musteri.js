const Musteri = require("../models2/musteri");
const Adres = require("../models2/adres");

exports.list = async (req, res) => {
    console.log(req.query.MusteriStatus)
    var  query = {}
    if (req.query.MusteriStatus) {
        query = {MusteriStatus: req.query.MusteriStatus}
    }
    const musteriler = await Musteri.find(query).populate('Sektoru', 'Adi')
    return res.status(200).json(musteriler)
}
  
exports.create = async (req, res) => {
    let payload = req.body;
    var musteri = new Musteri(payload)
    let row = await musteri.save()
    return res.status(200).json(row)
}

exports.getById = async (req, res) => {
    console.log(req.query.musteriId)
    const musteriler = await Musteri.find({_id: req.query.musteriId})
    return res.status(200).json(musteriler)
}

exports.update = async (req, res) => {
    console.log('musteri update')
    console.log(req.body)
    Musteri.findByIdAndUpdate(req.params.id, req.body, {new:true},
        (err, doc) => {
            if (err) {
                return res.status(404).json(null)
            }
            else {
                return res.status(200).json(doc)
            }
        })
}

exports.remove = async (req, res) => {
    Musteri.remove({_id: req.query.id}, (e) => {
        if (e) {
            return res.status(404).json(err)
        }
        else {
            return res.status(200).json('')
        }
    })
}

exports.adresEkle = async (req, res) => {
    console.log('adres ekle')
    const data = req.body
    const musteri = await Musteri.findById(req.params.musteriId)
    if (musteri) {
        let adres = await Adres({
            Musteri: musteri,
            AdresTuru: data.Turu,
            Adres1: data.Adres,
            Adres2: '',
            Ilce: '',
            Il: '',
            PostaKodu: ''
        })
        let row = await adres.save()
        console.log('adres eklendi')
        return res.status(200).json(row)
    }
}


exports.adresList = async (req, res) => {
    var ObjectId = require('mongoose').Types.ObjectId; 
    console.log('adres list')
    const musteri = await Musteri.findById(req.params.musteriId)
    if (musteri) {
        const adresler = await Adres.find({Musteri: new ObjectId(req.params.musteriId)})
        console.log(adresler)
        return res.status(200).json(adresler)
    }
}

exports.adresSil = async (req, res) => {
    var ObjectId = require('mongoose').Types.ObjectId; 
    const musteri = await Musteri.findById(req.params.musteriId)
    if (musteri) {
        await Adres.findByIdAndDelete(req.params.adresId)
        return res.status(200).json('')
    }
}