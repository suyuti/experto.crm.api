Musteri
    - Status (Aktif, Pasif, Potansiyel, Sorunlu)
    - FirmaUnvani
    - FirmaMarkasi
    - Temsilci (ref: user)
    - Kaynak (ref: MusteriKaynak)
    - Sektor (ref: Sektor)
    - IlgiliKisi (ref: Kisi)
    - Adres
    - il
    - ilce
    - OSB (ref: OSB)
    - Telefon
    - Web
    - CalisanSayisi
    - TeknikPersonelSayisi
    - MuhendisSayisi
    - Ciro
    - KobiMi
    - OncekiDestekler (ref: Destekler)
    - Danisman (ref: Danisman)
    // -----
    - VergiDairesi
    - VergiNo
    - EFaturaMi
    - TeknikIlgiliKisi (ref: Kisi)
    - MaliIlgiliKisi (ref: Kisi)
    - YontimIlgiliKisi (ref: Kisi)


Danisman:
    - Adi

Destekler:
    - Adi

Urun:
    - Adi
    - AktifMi
    - OnOdemeTutari
    - RaporBasiOdemeTutari
    - YuzdeTutari
    - SabitTutar
    - BasariTutari
    - KDVOrani


Teklif:
    - MusteriTemsilcisi (ref: user)
    - Urun (ref: urun)
    - Firma (ref: Musteri)
    - OnOdeme
    - IadeGarantiOran // sorulacak
    - IlgiliKisi (ref: Kisi)
    - KapanisTarihi
    - TeklifDurumu
    - Loglar [{status, date, mesaj}]

Kisi
    - Adi
    - Unvani
    - Cinsiyet
    - CepTel
    - Mail
    - IsTel
    - Dahili

GorusmeKayitlari:
    - Teklif (ref: Teklif)
    - Kanal [telefon, yuzyuze, mail, whatsapp]
    - Tarih
    - Kiminle (ref: Kisi)
    - Kim (ref: user)
    - Not

User:
    - Adi

MusteriKaynak
    [Reklam, Mailing, TemsilciReferansi, IcPersonel, Partner, Referans, Web, Fuar]

Sektor:
    - Adi

OSB:
    - Adi

Sozlesme:
    - SozlesmeNo
    - Teklif (ref: Teklif)
    - SozlesmeTarihi
    - SozlesmeSuresi



API
 Musteri

 GET    /musteri/potansiyel/list
 GET    /musteri/aktif/list
 GET    /musteri/pasif/list
 GET    /musteri/sorunlu/list

 GET    /musteri/:id



 //------------------------

https://www.shellhacks.com/mongodb-auth-enable-authentication-create-admin-root-user/

 sudo apt install -y mongodb
 sudo systemctl status mongodb
 mongo
 use admin

 > db.createUser(
  {
    user: "mongo-admin",
    pwd: "passw0rd",
    roles: [ { role: "userAdminAnyDatabase", db: "admin" } ]
  }
)

> db.createUser(
  {
    user: "mongo-root",
    pwd: "passw0rd",
    roles: [ { role: "root", db: "admin" } ]
  }
)
 

 Open MongoDB configuration file /etc/mongod.conf and enable auth:

security:
  authorization: "enabled"

  bind: 0.0.0.0

use ExpAPI ile db olustur
db.createUser() olustur
